FROM openjdk:11
ADD target/MasivianRouletteAPI-1.0.0.jar MasivianRouletteAPI-1.0.0.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "MasivianRouletteAPI-1.0.0.jar"]