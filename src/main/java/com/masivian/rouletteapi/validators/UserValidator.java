package com.masivian.rouletteapi.validators;

import com.masivian.rouletteapi.dtos.LoadWalletRequest;
import com.masivian.rouletteapi.dtos.LoginRequestDTO;
import com.masivian.rouletteapi.entity.User;
import com.masivian.rouletteapi.exceptions.ValidateServiceException;

public class UserValidator {

	public static void save(User user) {
		
		if (user.getUsername() == null || user.getUsername().trim().isEmpty()) {
			throw new ValidateServiceException("El nombre de usuario es requerido");
		}
		
		if (user.getMoneyAvailable() < 0) {
			throw new ValidateServiceException("El dinero disponible es incorrecto");
		}
		
		if (user.getMoneyAvailable() == 0) {
			throw new ValidateServiceException("El dinero disponible NO puede ser cero");
		}
		
		if (String.valueOf(user.getMoneyAvailable()).trim() == null || String.valueOf(user.getMoneyAvailable()).trim().isEmpty()) {
			throw new ValidateServiceException("El dinero disponible es requerido");
		}
		
		if (user.getPassword() == null || user.getPassword().trim().isEmpty()) {
			throw new ValidateServiceException("La contrasena es requerida");
		}
	}
	
	public static void login(LoginRequestDTO request) {
		
		if (request.getUsername() == null || request.getUsername().isEmpty()) {
			throw new ValidateServiceException("El nombre de usuario es requerido");
		}
		
		if (request.getPassword() == null || request.getPassword().isEmpty()) {
			throw new ValidateServiceException("La contrasena es requesrida");
		}
		
	}
	
	public static void loadWallet(LoadWalletRequest request) {
		
		if (request.getQuantity() == null) {
			throw new ValidateServiceException("La cantidad es requerida");
		}
		
		if (request.getQuantity() > 5000) {
			throw new ValidateServiceException("No puede cargar mas de $5.000 USD en una sola transferencia");
		}
		
		if (request.getQuantity() <= 0) {
			throw new ValidateServiceException("La cantidad no puede ser menor o igual a cero");
		}
	}
}
