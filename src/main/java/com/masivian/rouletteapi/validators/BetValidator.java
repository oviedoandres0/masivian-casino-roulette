package com.masivian.rouletteapi.validators;

import com.masivian.rouletteapi.entity.Bet;
import com.masivian.rouletteapi.exceptions.ValidateServiceException;

public class BetValidator {
	
	public static void save(Bet bet) {
		
		if (bet.getQuantity() == null) {
			throw new ValidateServiceException("La catidad a apostar es requerida");
		}
		
		if (bet.getQuantity() > 10000) {
			throw new ValidateServiceException("No puede apostar mas de $10.000 USD");
		}
		
		if (bet.getQuantity() <= 0) {
			throw new ValidateServiceException("La cantidad NO puede ser menor o igual a cero");
		}
		
		if (bet.getBet() == null || bet.getBet().trim().isEmpty()) {
			throw new ValidateServiceException("La apustesta es requerida.");
		}
		
		if (bet.getBetType() == null || bet.getBetType().trim().isEmpty()) {
			throw new ValidateServiceException("El tipo de la apuesta es requerido.");
		}
		
		if (bet.getBetType().toUpperCase().equals("COLOR")) {
			if (!bet.getBet().trim().toUpperCase().equals("ROJO") && !bet.getBet().trim().toUpperCase().equals("NEGRO")) {
				throw new ValidateServiceException("El color debe ser ROJO o NEGRO");
			}
		}
		
		if (bet.getBetType().toUpperCase().equals("NUMERO")) {
			if (Integer.parseInt(bet.getBet()) <= 0 || Integer.parseInt(bet.getBet()) > 36) {
				throw new ValidateServiceException("No es un numero valido, debe estar entre 1 y 36");
			}
		}
		
		if (!bet.getBetType().toUpperCase().equals("NUMERO") && !bet.getBetType().toUpperCase().equals("COLOR")) {
			throw new ValidateServiceException("El tipo de apuesta debe ser: NUMERO o COLOR");
		}
		
		if (bet.getRoulette() == null || bet.getRoulette().getId() == null) {
			throw new ValidateServiceException("Debe agregar una mesa a la cual apostar y esta debe estar abierta.");
		}
		
	}
}
