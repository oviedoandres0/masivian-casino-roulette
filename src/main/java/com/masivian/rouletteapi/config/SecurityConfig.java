package com.masivian.rouletteapi.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.masivian.rouletteapi.security.RestAuthenticationEntryPoint;
import com.masivian.rouletteapi.security.TokenAuthenticationFilter;


@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
		.cors()
			.and()
			.sessionManagement()
			.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
			.and()
		.csrf()
			.disable()
		.formLogin()
			.disable()
		.httpBasic()
			.disable()
		.exceptionHandling()
			.authenticationEntryPoint(new RestAuthenticationEntryPoint())
			.and()
		.authorizeRequests()
			.antMatchers("/",
					"/error",
					"/fabicon.ico",
					"/**/*.png",
					"/**/*.gif",
					"/**/*.svg",
					"/**/*.jpg",
					"/**/*.html",
					"/**/*.css",
					"/**/*.js",
					"/**/*.woff2")
				.permitAll()
			.antMatchers(
					"/users/login",
					"/v2/api-docs",
					"/webjars/**",
					"/swagger-resources/**"
					)
				.permitAll()
			.anyRequest()
				.authenticated();
		http
			.addFilterBefore(createTokenAutheticationFilter(), UsernamePasswordAuthenticationFilter.class);
	}
	
	@Bean
	public TokenAuthenticationFilter createTokenAutheticationFilter() {
		return new TokenAuthenticationFilter();
	}

	@Bean
	public PasswordEncoder createPasswordEncoded() {
		return new BCryptPasswordEncoder();
	}
}
