package com.masivian.rouletteapi.config;

import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.masivian.rouletteapi.converters.BetConverter;
import com.masivian.rouletteapi.converters.RouletteConverter;
import com.masivian.rouletteapi.converters.RouletteDetailConverter;
import com.masivian.rouletteapi.converters.RoulettePosibilityConverter;
import com.masivian.rouletteapi.converters.UserConverter;

@Configuration
public class ConverterConfig {

	
	@Value("${config.datetimeFormat}")
	private String dateTimeFormat;

	@Bean
	public BetConverter getBetConverter() {
		DateTimeFormatter format = DateTimeFormatter.ofPattern(dateTimeFormat);
		return new BetConverter(format, getRouletteConverter(), getUserConverter());
	}
	
	@Bean
	public RouletteConverter getRouletteConverter() {
		DateTimeFormatter format = DateTimeFormatter.ofPattern(dateTimeFormat);
		return new RouletteConverter(format);
	}
	
	@Bean
	public RouletteDetailConverter getRouletteDetailConverter() {
		return new RouletteDetailConverter();
	}
	
	@Bean
	public UserConverter getUserConverter() {
		return new UserConverter();
	}
	
	@Bean 
	public RoulettePosibilityConverter getRoulettePosibilityConverter() {
		return new RoulettePosibilityConverter();
	}
}
