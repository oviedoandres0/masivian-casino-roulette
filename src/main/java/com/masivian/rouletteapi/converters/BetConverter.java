package com.masivian.rouletteapi.converters;

import java.time.format.DateTimeFormatter;

import com.masivian.rouletteapi.dtos.BetDTO;
import com.masivian.rouletteapi.dtos.RouletteDTO;
import com.masivian.rouletteapi.entity.Bet;
import com.masivian.rouletteapi.entity.Roulette;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class BetConverter extends AbstractConverter<Bet, BetDTO>{

	private DateTimeFormatter dateTimeFormat;
	private RouletteConverter rouletteConverter;
	private UserConverter userConverter;
	
	@Override
	public BetDTO convertEntityToDTO(Bet entity) {
		if (entity == null) return null;
		
		RouletteDTO dtoRoulette = rouletteConverter.convertEntityToDTO(entity.getRoulette());
		
		return BetDTO.builder()
				.id(entity.getId())
				.quantity(entity.getQuantity())
				.bet(entity.getBet())
				.betType(entity.getBetType())
				.regDate(dateTimeFormat.format(entity.getRegDate()))
				.roulette(dtoRoulette)
				.user(userConverter.convertEntityToDTO(entity.getUser()))
				.build();
	}

	@Override
	public Bet convertDTOToEntity(BetDTO dto) {
		if (dto == null) return null;
		
		Roulette roulette = rouletteConverter.convertDTOToEntity(dto.getRoulette());
		
		return Bet.builder()
				.id(dto.getId())
				.quantity(dto.getQuantity())
				.bet(dto.getBet())
				.betType(dto.getBetType())
				.roulette(roulette)
				.user(userConverter.convertDTOToEntity(dto.getUser()))
				.build();
	}

}
