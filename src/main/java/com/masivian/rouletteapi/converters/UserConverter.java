package com.masivian.rouletteapi.converters;

import com.masivian.rouletteapi.dtos.SignUpRequestDTO;
import com.masivian.rouletteapi.dtos.UserDTO;
import com.masivian.rouletteapi.entity.User;

public class UserConverter extends AbstractConverter<User, UserDTO> {

	@Override
	public UserDTO convertEntityToDTO(User entity) {
		return UserDTO.builder()
					.id(entity.getId())
					.moneyAvailable(entity.getMoneyAvailable())
					.username(entity.getUsername())
					.build();
	}

	@Override
	public User convertDTOToEntity(UserDTO dto) {
		return User.builder()
				.id(dto.getId())
				.moneyAvailable(dto.getMoneyAvailable())
				.username(dto.getUsername())
				.build();
	}

	public User signup(SignUpRequestDTO dto) {
		if (dto == null) return null;
		
		return User.builder()
				.moneyAvailable(10000)
				.username(dto.getUsername())
				.password(dto.getPassword())
				.build();
	}
}
