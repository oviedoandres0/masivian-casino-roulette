package com.masivian.rouletteapi.converters;

import com.masivian.rouletteapi.dtos.RoulettePosibilityDTO;
import com.masivian.rouletteapi.entity.RoulettePosibility;

public class RoulettePosibilityConverter extends AbstractConverter<RoulettePosibility, RoulettePosibilityDTO>{

	@Override
	public RoulettePosibilityDTO convertEntityToDTO(RoulettePosibility entity) {
		if (entity == null) return null;
		
		return RoulettePosibilityDTO.builder()
				.number(entity.getNumber())
				.color(entity.getColor())
				.selected(entity.isSelected())
				.build();
	}

	@Override
	public RoulettePosibility convertDTOToEntity(RoulettePosibilityDTO dto) {
		if (dto == null) return null;
		
		return RoulettePosibility.builder()
				.number(dto.getNumber())
				.color(dto.getColor())
				.selected(dto.isSelected())
				.build();
	}

}
