package com.masivian.rouletteapi.converters;

import java.time.format.DateTimeFormatter;

import com.masivian.rouletteapi.dtos.RouletteDTO;
import com.masivian.rouletteapi.entity.Roulette;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class RouletteConverter extends AbstractConverter<Roulette, RouletteDTO> {

	private DateTimeFormatter dateTimeFormat;
	
	@Override
	public RouletteDTO convertEntityToDTO(Roulette entity) {
		if (entity == null) return null;

		return RouletteDTO.builder()
				.id(entity.getId())
				.regDate(dateTimeFormat.format(entity.getRegDate()))
				.open(entity.isOpen())
				.finished(entity.isFinished())
				//.roulettePosibilities(dtoPosibilities)
				.build();
	}

	@Override
	public Roulette convertDTOToEntity(RouletteDTO dto) {
		if (dto == null) return null;
		
		//List<RoulettePosibility> posibilities = convertPositibiliesDTOListToEntityList(dto.getRoulettePosibilities());
		
		return Roulette.builder()
				.id(dto.getId())
				.open(dto.isOpen())
				.finished(dto.isFinished())
				//.roulettePosibilities(posibilities)
				.build();
	}
}
