package com.masivian.rouletteapi.converters;

import java.util.ArrayList;
import java.util.List;

import com.masivian.rouletteapi.dtos.RoulettePosibilityDTO;
import com.masivian.rouletteapi.dtos.RoulettePosibilityDetailDTO;
import com.masivian.rouletteapi.entity.Roulette;
import com.masivian.rouletteapi.entity.RoulettePosibility;

public class RouletteDetailConverter extends AbstractConverter<Roulette, RoulettePosibilityDetailDTO> {

	@Override
	public RoulettePosibilityDetailDTO convertEntityToDTO(Roulette entity) {
		if (entity == null) return null;
		
		List<RoulettePosibilityDTO> dtoPosibilities = convertPosibilitiesEntityListToDTOList(entity.getRoulettePosibilities());
		
		return RoulettePosibilityDetailDTO.builder()
				.rouletteId(entity.getId())
				.open(entity.isOpen())
				.finished(entity.isFinished())
				.posibilities(dtoPosibilities)
				.build();
	}

	@Override
	public Roulette convertDTOToEntity(RoulettePosibilityDetailDTO dto) {
		if (dto == null) return null;
		
		List<RoulettePosibility> posibilities = convertPositibiliesDTOListToEntityList(dto.getPosibilities());
		
		return Roulette.builder()
				.id(dto.getRouletteId())
				.open(dto.isOpen())
				.finished(dto.isFinished())
				.roulettePosibilities(posibilities)
				.build();
	}

	public List<RoulettePosibilityDTO> convertPosibilitiesEntityListToDTOList(List<RoulettePosibility> posibilities) {
		if (posibilities == null) return null;
		
		List<RoulettePosibilityDTO> dtos = new ArrayList<>();
		
		for (RoulettePosibility posibility : posibilities) {
			dtos.add(
					RoulettePosibilityDTO.builder()
						.number(posibility.getNumber())
						.color(posibility.getColor())
						.selected(posibility.isSelected())
						.build()
				);
		}
		
		return dtos;
	}
	
	public List<RoulettePosibility> convertPositibiliesDTOListToEntityList(List<RoulettePosibilityDTO> dtos) {
		if (dtos == null) return null;
		
		List<RoulettePosibility> entities = new ArrayList<>();
		
		for (RoulettePosibilityDTO dto : dtos) {
			entities.add(
					RoulettePosibility.builder()
						.number(dto.getNumber())
						.color(dto.getColor())
						.selected(dto.isSelected())
						.build()
				);
		}
		
		return entities;
	}
}
