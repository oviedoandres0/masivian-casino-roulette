package com.masivian.rouletteapi.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "USERS")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "MONEY_AVAILABLE", nullable = false)
	private double moneyAvailable;
	
	@Column(name = "USERNAME", length = 30, nullable = false)
	private String username;
	
	@Column(name = "PASSWORD", length = 150, nullable = false)
	private String password;
}
