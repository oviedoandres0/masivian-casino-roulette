package com.masivian.rouletteapi.entity;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Entity
@Table(name = "ROULETTES")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Roulette {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "REG_DATE", nullable = false)
	private LocalDateTime regDate;
	
	@Column(name = "OPEN", nullable = false)
	private boolean open;
	
	@Column(name = "FINISHED", nullable = false)
	private boolean finished;
	
	@OneToMany(mappedBy = "roulette", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<RoulettePosibility> roulettePosibilities;
}
