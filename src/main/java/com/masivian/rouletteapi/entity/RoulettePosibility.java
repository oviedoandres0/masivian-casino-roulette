package com.masivian.rouletteapi.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "ROULETE_POSIBILITIES")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RoulettePosibility {

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "NUMBER", nullable = false)
	private int number;
	
	@Column(name = "COLOR", nullable = false, length = 6)
	private String color;
	
	@Column(name = "SELECTED", nullable = false)
	private boolean selected;
	
	@ManyToOne
	@JoinColumn(name = "FK_ROULETTE")
	private Roulette roulette;
}
