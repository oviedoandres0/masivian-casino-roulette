package com.masivian.rouletteapi.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "BETS")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Bet {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "QUANTITY", nullable = false)
	private Double quantity;
	
	@Column(name = "BET", nullable = false)
	private String bet;
	
	@Column(name = "BET_TYPE", nullable = false)
	private String betType;
	
	@Column(name = "REG_DATE", nullable = false)
	private LocalDateTime regDate;
	
	@ManyToOne
	@JoinColumn(name = "FK_ROULETTE", updatable = false)
	private Roulette roulette;
	
	@ManyToOne
	@JoinColumn(name = "FK_USER", updatable = false)
	private User user;
}
