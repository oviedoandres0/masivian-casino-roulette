package com.masivian.rouletteapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MasivianRouletteApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MasivianRouletteApiApplication.class, args);
	}

}
