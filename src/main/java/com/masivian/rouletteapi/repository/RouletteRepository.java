package com.masivian.rouletteapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.masivian.rouletteapi.entity.Roulette;

@Repository
public interface RouletteRepository extends JpaRepository<Roulette, Long>{

}
