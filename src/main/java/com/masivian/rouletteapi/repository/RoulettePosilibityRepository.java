package com.masivian.rouletteapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.masivian.rouletteapi.entity.RoulettePosibility;

@Repository
public interface RoulettePosilibityRepository extends JpaRepository<RoulettePosibility, Long>{

}
