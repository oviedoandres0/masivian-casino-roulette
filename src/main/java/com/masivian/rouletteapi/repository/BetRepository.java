package com.masivian.rouletteapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.masivian.rouletteapi.entity.Bet;

public interface BetRepository extends JpaRepository<Bet, Long>{

	public List<Bet> findByRouletteId(Long rouletteId);
	
	@Query("SELECT b FROM Bet b WHERE b.betType = ?1 AND b.roulette.id = ?2 AND b.bet = ?3")
	public List<Bet> findByBetTypeAndRouletteIdAndBet(String betType, Long rouletteId, String bet);
	
}
