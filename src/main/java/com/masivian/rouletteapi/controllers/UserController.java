package com.masivian.rouletteapi.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.masivian.rouletteapi.converters.UserConverter;
import com.masivian.rouletteapi.dtos.LoadWalletRequest;
import com.masivian.rouletteapi.dtos.LoadWalletResponse;
import com.masivian.rouletteapi.dtos.LoginRequestDTO;
import com.masivian.rouletteapi.dtos.LoginResponseDTO;
import com.masivian.rouletteapi.dtos.SignUpRequestDTO;
import com.masivian.rouletteapi.dtos.UserDTO;
import com.masivian.rouletteapi.entity.User;
import com.masivian.rouletteapi.service.UserService;
import com.masivian.rouletteapi.util.WrapperResponse;

@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	private UserService userService;
	
	@Autowired
	private UserConverter userConverter;
	
	@PostMapping("/signup")
	public ResponseEntity<WrapperResponse<UserDTO>> signup(@RequestBody SignUpRequestDTO request) {
		User user = userService.createUser(userConverter.signup(request));
		UserDTO dtoUser = userConverter.convertEntityToDTO(user);
		
		return new WrapperResponse<>(true, "Usuario creado con exito", dtoUser)
				.createResponse(HttpStatus.CREATED);
	}
	
	@PostMapping("/login")
	public ResponseEntity<WrapperResponse<LoginResponseDTO>> login(@RequestBody LoginRequestDTO request) {
		LoginResponseDTO response = userService.login(request);
		return new WrapperResponse<>(true, "Login exitoso", response)
				.createResponse(HttpStatus.OK);
	}
	
	@GetMapping("/showuserdetails")
	public ResponseEntity<WrapperResponse<UserDTO>> showUserDetails() {
		User user = userService.showUserDetails();
		UserDTO dtoUser = userConverter.convertEntityToDTO(user);
		
		return new WrapperResponse<>(true, "Detalles del Usuario", dtoUser)
				.createResponse(HttpStatus.CREATED);
	}
	
	@PutMapping("/loadwallet")
	public ResponseEntity<WrapperResponse<LoadWalletResponse>> loadWallet(@RequestBody LoadWalletRequest request) {
		LoadWalletResponse response = userService.loadWallet(request);
		
		return new WrapperResponse<>(true, "Transaccion Exitosa", response)
				.createResponse(HttpStatus.CREATED);
	}
}
