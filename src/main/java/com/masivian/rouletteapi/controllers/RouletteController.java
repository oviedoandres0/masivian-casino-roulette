package com.masivian.rouletteapi.controllers;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.masivian.rouletteapi.converters.RouletteConverter;
import com.masivian.rouletteapi.converters.RouletteDetailConverter;
import com.masivian.rouletteapi.dtos.ResultDTO;
import com.masivian.rouletteapi.dtos.RouletteDTO;
import com.masivian.rouletteapi.dtos.RoulettePosibilityDetailDTO;
import com.masivian.rouletteapi.entity.Roulette;
import com.masivian.rouletteapi.service.RouletteService;
import com.masivian.rouletteapi.util.WrapperResponse;

@RestController
@RequestMapping("roulettes")
public class RouletteController {
	
	@Autowired
	private RouletteService rouletteService;
	
	@Autowired
	private RouletteConverter rouletteConverter;
	
	@Autowired
	private RouletteDetailConverter rouletteDetailConverter;
	
	@GetMapping
	public ResponseEntity<WrapperResponse<List<RouletteDTO>>> findAll(
			@RequestParam(value = "pageNumber", required = false, defaultValue = "0") int pageNumber,
			@RequestParam(value = "pageSize", required = false, defaultValue = "5") int pageSize
		) {
		Pageable page = PageRequest.of(pageNumber, pageSize);
		List<Roulette> roulettes = rouletteService.findAll(page);
		List<RouletteDTO> dtoProducts = rouletteConverter.convertEntityListToDTOList(roulettes);
		
		return new WrapperResponse<>(true, "Ruletas encontradas", dtoProducts)
				.createResponse(HttpStatus.OK);
	}
	
	@GetMapping("/{rouletteId}")
	public ResponseEntity<WrapperResponse<RouletteDTO>> findById(@PathVariable("rouletteId") Long rouletteId) {
		Roulette roulette = rouletteService.findById(rouletteId);
		RouletteDTO dtRoulette = rouletteConverter.convertEntityToDTO(roulette);
		
		return new WrapperResponse<>(true, "Ruleta encontrada", dtRoulette)
				.createResponse(HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<WrapperResponse<RouletteDTO>> createNewRoulette() {
		Roulette newRoulette = rouletteService.createNewRoulette(
				Roulette.builder()
					.regDate(LocalDateTime.now())
					.open(false)
					.finished(false)
					.build()
				);
		RouletteDTO dtoNewRoulette = rouletteConverter.convertEntityToDTO(newRoulette);
		
		return new WrapperResponse<>(true, "Ruleta creada con exito", dtoNewRoulette)
				.createResponse(HttpStatus.CREATED);
	}
	
	@PutMapping("/{rouletteId}")
	public ResponseEntity<WrapperResponse<RouletteDTO>> openRoulette(@PathVariable("rouletteId") Long rouletteId) {
		Roulette selectedRoulette = rouletteService.openRoulette(rouletteId);
		RouletteDTO dtoSelectedRoulette = rouletteConverter.convertEntityToDTO(selectedRoulette);
		
		return new WrapperResponse<>(true, "Ruleta abierta con exito", dtoSelectedRoulette)
				.createResponse(HttpStatus.OK);
	}
	
	@PutMapping("/{rouletteId}/viewResults")
	public ResponseEntity<WrapperResponse<ResultDTO>> closeRoulette(@PathVariable("rouletteId") Long rouletteId) {
		ResultDTO dtoResult = rouletteService.closeRoulette(rouletteId);
		
		return new WrapperResponse<>(true, "Ruleta cerrada con exito", dtoResult)
				.createResponse(HttpStatus.OK);
	}
	
	@GetMapping("/{rouletteId}/viewPosibilities")
	public ResponseEntity<WrapperResponse<RoulettePosibilityDetailDTO>> viewPosibilities(@PathVariable("rouletteId") Long rouletteId) {
		Roulette roulette = rouletteService.viewRoulettePosibilities(rouletteId);
		RoulettePosibilityDetailDTO dtoDetails = rouletteDetailConverter.convertEntityToDTO(roulette);
		
		return new WrapperResponse<>(true, "Posibilidades de la ruleta", dtoDetails)
				.createResponse(HttpStatus.OK);
	}
}
