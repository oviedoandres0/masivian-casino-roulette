package com.masivian.rouletteapi.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.masivian.rouletteapi.converters.BetConverter;
import com.masivian.rouletteapi.dtos.BetDTO;
import com.masivian.rouletteapi.entity.Bet;
import com.masivian.rouletteapi.service.BetService;
import com.masivian.rouletteapi.util.WrapperResponse;

@RestController
@RequestMapping("bets")
public class BetController {

	@Autowired
	private BetService betService;
	
	@Autowired
	private BetConverter betConverter;
	
	@GetMapping
	public ResponseEntity<WrapperResponse<List<BetDTO>>> findAll(
			@RequestParam(value = "pageNumber", required = false, defaultValue = "0") int pageNumber,
			@RequestParam(value = "pageSize", required = false, defaultValue = "5") int pageSize
		) {
		Pageable page = PageRequest.of(pageNumber, pageSize);
		List<Bet> bets = betService.findAll(page);
		List<BetDTO> dtoBets = betConverter.convertEntityListToDTOList(bets);
		
		return new WrapperResponse<>(true, "Apuestas encontradas", dtoBets)
				.createResponse(HttpStatus.OK);
	}
	
	@GetMapping("/{betId}")
	public ResponseEntity<WrapperResponse<BetDTO>> findById(@PathVariable("betId") Long betId) {
		Bet bet = betService.findById(betId);
		BetDTO dtoBet = betConverter.convertEntityToDTO(bet);
		
		return new WrapperResponse<>(true, "Apuesta encontrada", dtoBet)
				.createResponse(HttpStatus.OK);
	}
	
	@GetMapping("/roulette/{rouletteId}")
	public ResponseEntity<WrapperResponse<List<BetDTO>>> findByRouletteId(@PathVariable("rouletteId") Long rouletteId) {
		List<Bet> bets = betService.findByRouletteId(rouletteId);
		List<BetDTO> dtoBets = betConverter.convertEntityListToDTOList(bets);
		
		return new WrapperResponse<>(true, "Apuestas encontradas", dtoBets)
				.createResponse(HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<WrapperResponse<BetDTO>> createNewBet(@RequestBody Bet bet) {
		Bet newBet = betService.save(bet);
		BetDTO dtoNewDTO = betConverter.convertEntityToDTO(newBet);
		
		return new WrapperResponse<>(true, "Apuesta creada con exito", dtoNewDTO)
				.createResponse(HttpStatus.CREATED);
	}
	
}
