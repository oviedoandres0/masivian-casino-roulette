package com.masivian.rouletteapi.dtos;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ResultDTO {

	private Long rouletteId;
	private List<BetDTO> allBets;
	private RoulettePosibilityDTO winnerPosibility;
	private List<BetDTO> winnerBets;
}
