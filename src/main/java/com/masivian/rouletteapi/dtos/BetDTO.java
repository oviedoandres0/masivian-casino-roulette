package com.masivian.rouletteapi.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BetDTO {
	private Long id;
	private Double quantity;
	private String bet;
	private String betType;
	private String regDate;
	private RouletteDTO roulette;
	private UserDTO user;
}
