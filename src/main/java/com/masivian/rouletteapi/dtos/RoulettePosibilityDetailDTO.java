package com.masivian.rouletteapi.dtos;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RoulettePosibilityDetailDTO {

	private Long rouletteId;
	private boolean open;
	private boolean finished;
	List<RoulettePosibilityDTO> posibilities;
}
