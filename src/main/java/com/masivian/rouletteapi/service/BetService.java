package com.masivian.rouletteapi.service;

import java.util.List;

import org.springframework.data.domain.Pageable;
import com.masivian.rouletteapi.entity.Bet;
import com.masivian.rouletteapi.entity.Roulette;

public interface BetService {

	public Bet save(Bet bet);
	public List<Bet> findAll(Pageable page);
	public List<Bet> findByRouletteId(Long rouletteId);
	public Bet findById(Long betId);
	public boolean selectPosibility(Bet bet, Roulette roulette);
}
