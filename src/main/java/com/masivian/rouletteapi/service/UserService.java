package com.masivian.rouletteapi.service;

import com.masivian.rouletteapi.dtos.LoadWalletRequest;
import com.masivian.rouletteapi.dtos.LoadWalletResponse;
import com.masivian.rouletteapi.dtos.LoginRequestDTO;
import com.masivian.rouletteapi.dtos.LoginResponseDTO;
import com.masivian.rouletteapi.entity.User;

public interface UserService {
	public User createUser(User user);
	public LoginResponseDTO login(LoginRequestDTO request);
	public String createToken(User user);
	public boolean validateToken(String token);
	public String getUsernameFromToken(String jwt);
	public User showUserDetails();
	public LoadWalletResponse loadWallet(LoadWalletRequest request);
}
