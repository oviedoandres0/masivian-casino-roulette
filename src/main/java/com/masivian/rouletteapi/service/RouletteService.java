package com.masivian.rouletteapi.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.masivian.rouletteapi.dtos.ResultDTO;
import com.masivian.rouletteapi.entity.Bet;
import com.masivian.rouletteapi.entity.Roulette;
import com.masivian.rouletteapi.entity.RoulettePosibility;

public interface RouletteService {

	public List<Roulette> findAll(Pageable page);
	public Roulette findById(Long rouletteId);
	public Roulette createNewRoulette(Roulette roulette);
	public Roulette viewRoulettePosibilities(Long rouletteId);
	public Roulette openRoulette(Long rouletteId);
	public RoulettePosibility getWinningChance(Roulette roulette);
	public ResultDTO closeRoulette(Long rouletteId);
	public void calculateReward(String betType, List<Bet> bets);
}
