package com.masivian.rouletteapi.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.masivian.rouletteapi.converters.UserConverter;
import com.masivian.rouletteapi.dtos.LoadWalletRequest;
import com.masivian.rouletteapi.dtos.LoadWalletResponse;
import com.masivian.rouletteapi.dtos.LoginRequestDTO;
import com.masivian.rouletteapi.dtos.LoginResponseDTO;
import com.masivian.rouletteapi.entity.User;
import com.masivian.rouletteapi.exceptions.GeneralServiceException;
import com.masivian.rouletteapi.exceptions.NoDataFoundException;
import com.masivian.rouletteapi.exceptions.ValidateServiceException;
import com.masivian.rouletteapi.repository.UserRepository;
import com.masivian.rouletteapi.security.UserPrincipal;
import com.masivian.rouletteapi.service.UserService;
import com.masivian.rouletteapi.validators.UserValidator;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UserServiceImpl implements UserService {

	@Value("${jwt.password}")
	private String jwtSecret;
	
	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private PasswordEncoder passwordEncoded;
	
	@Autowired
	private UserConverter userConverter;
	
	@Override
	public User createUser(User user) {
		try {
			// Validate the user
			UserValidator.save(user);
			
			User existUser = userRepo.findByUsername(user.getUsername())
					.orElse(null);
			if (existUser != null) throw new ValidateServiceException("El Nombre de Usuario ya existe");
			
			user.setPassword(passwordEncoded.encode(user.getPassword()));
			return userRepo.save(user);
		} catch (ValidateServiceException | NoDataFoundException e) {
			log.info(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new GeneralServiceException(e.getMessage(), e);
		}
	}

	@Override
	public LoginResponseDTO login(LoginRequestDTO request) {
		try {
			// Validate the login
			UserValidator.login(request);
			
			User user = userRepo.findByUsername(request.getUsername())
					.orElseThrow(() -> new NoDataFoundException("Usuario y/o Contrasena Incorrectos"));
			
			if(!passwordEncoded.matches(request.getPassword(), user.getPassword()))
				throw new ValidateServiceException("Usuario y/o Contrasena Incorrectos");
			
			return LoginResponseDTO.builder()
					.user(userConverter.convertEntityToDTO(user))
					.token(createToken(user))
					.build();
			
		} catch (ValidateServiceException | NoDataFoundException e) {
			log.info(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new GeneralServiceException(e.getMessage(), e);
		}
	}

	@Override
	public String createToken(User user) {
		Date now = new Date();
		Date expirationDate = new Date(now.getTime() + (1000*60*60));
		
		return Jwts.builder()
				.setSubject(user.getUsername())
				.setIssuedAt(now)
				.setExpiration(expirationDate)
				.signWith(SignatureAlgorithm.HS512, jwtSecret)
				.compact();
	}

	@Override
	public boolean validateToken(String token) {
		try {
			Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token);
			return true;
		} catch (UnsupportedJwtException e) {
			log.error("JWT in a particular format/configuration that does not match the format expected by the application.");
		} catch(MalformedJwtException e) {
			log.error("JWT was not correctly constructed and should be rejected.");
		} catch(SignatureException e) {
			log.error("Exception indicating that either calculating a signature or verifying an existing signature of a JWT failed.");
		} catch(ExpiredJwtException e) {
			log.error("Exception indicating that a JWT was accepted after it expired and must be rejected.");
		}
		return false;
	}

	@Override
	public String getUsernameFromToken(String jwt) {
		try {
			return Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(jwt).getBody().getSubject();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new ValidateServiceException("Invalid Yoken");
		}
	}

	@Override
	public User showUserDetails() {
		try {
			// Get the logged user
			User user = UserPrincipal.getUserEntity();
			
			return user;
		} catch (ValidateServiceException | NoDataFoundException e) {
			log.info(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new GeneralServiceException(e.getMessage(), e);
		}
	}

	@Override
	public LoadWalletResponse loadWallet(LoadWalletRequest request) {
		try {
			// Get the logged user
			User user = UserPrincipal.getUserEntity();
			
			UserValidator.loadWallet(request);
			
			user.setMoneyAvailable(user.getMoneyAvailable() + request.getQuantity());
			userRepo.save(user);
			
			return LoadWalletResponse.builder()
					.username(user.getUsername())
					.newBalance(user.getMoneyAvailable())
					.build();
		} catch (ValidateServiceException | NoDataFoundException e) {
			log.info(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new GeneralServiceException(e.getMessage(), e);
		}
	}

}
