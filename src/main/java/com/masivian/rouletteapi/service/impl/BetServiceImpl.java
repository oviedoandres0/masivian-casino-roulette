package com.masivian.rouletteapi.service.impl;

import java.time.LocalDateTime;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.masivian.rouletteapi.entity.Bet;
import com.masivian.rouletteapi.entity.Roulette;
import com.masivian.rouletteapi.entity.RoulettePosibility;
import com.masivian.rouletteapi.entity.User;
import com.masivian.rouletteapi.exceptions.GeneralServiceException;
import com.masivian.rouletteapi.exceptions.NoDataFoundException;
import com.masivian.rouletteapi.exceptions.ValidateServiceException;
import com.masivian.rouletteapi.repository.BetRepository;
import com.masivian.rouletteapi.repository.RoulettePosilibityRepository;
import com.masivian.rouletteapi.repository.RouletteRepository;
import com.masivian.rouletteapi.repository.UserRepository;
import com.masivian.rouletteapi.security.UserPrincipal;
import com.masivian.rouletteapi.service.BetService;
import com.masivian.rouletteapi.validators.BetValidator;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class BetServiceImpl implements BetService {

	@Autowired
	private BetRepository betRepo;
	
	@Autowired
	private RouletteRepository rouletteRepo;
	
	@Autowired
	private RoulettePosilibityRepository roulettePosilibityRepo;
	
	@Autowired
	private UserRepository userRepo;
	
	@Transactional
	@Override
	public Bet save(Bet bet) {
		try {
			// Validate Params
			BetValidator.save(bet);
			
			// Get the logged user
			User user = UserPrincipal.getUserEntity();
			
			Roulette roulette = rouletteRepo.findById(bet.getRoulette().getId())
					.orElseThrow(() -> new NoDataFoundException("La Ruleta NO existe."));

			if (!roulette.isOpen()) throw new ValidateServiceException("No puede apostar en una ruleta que no este abierta");
			if (roulette.isFinished()) throw new ValidateServiceException("No puede apostar en una ruleta finlizada");
			
			
			bet.setRegDate(LocalDateTime.now());
			bet.setRoulette(roulette);
			bet.setUser(user);
			
			// Select the roulette posibility and update
			boolean possibilityAvailable = selectPosibility(bet, roulette);
			
			if (possibilityAvailable) {
				double moneyAvailableDiff = user.getMoneyAvailable() - bet.getQuantity();
				
				if (moneyAvailableDiff > 0) {
					user.setMoneyAvailable(moneyAvailableDiff);
					userRepo.save(user);
				} else {
					throw new ValidateServiceException("No Dispone de los fondos para realizar esta apuesta");
				}
				
				return betRepo.save(bet);
			} else {
				throw new ValidateServiceException("No se pueda apostar por una propiedad que ya esta seleccionada");
			}
		} catch (ValidateServiceException | NoDataFoundException e) {
			log.info(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new GeneralServiceException(e.getMessage(), e);
		}
	}

	@Override
	public List<Bet> findAll(Pageable page) {
		try {
			return betRepo.findAll(page).toList();
		} catch (ValidateServiceException | NoDataFoundException e) {
			log.info(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new GeneralServiceException(e.getMessage(), e);
		}
	}

	@Override
	public List<Bet> findByRouletteId(Long rouletteId) {
		try {
			return betRepo.findByRouletteId(rouletteId);
		} catch (ValidateServiceException | NoDataFoundException e) {
			log.info(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new GeneralServiceException(e.getMessage(), e);
		}
	}

	@Override
	public Bet findById(Long betId) {
		try {
			return betRepo.findById(betId)
					.orElseThrow(() -> new NoDataFoundException("La apuesta NO existe"));
		} catch (ValidateServiceException | NoDataFoundException e) {
			log.info(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new GeneralServiceException(e.getMessage(), e);
		}	
	}

	@Transactional
	@Override
	public boolean selectPosibility(Bet bet, Roulette roulette) {
		try {
			RoulettePosibility posibilityToSelect = null;
			if (bet.getBetType().trim().toUpperCase().equals("NUMERO")) {
				int betNumber = Integer.parseInt(bet.getBet());
				for (RoulettePosibility posibility : roulette.getRoulettePosibilities()) {
					if (posibility.getNumber() == betNumber) {
						posibilityToSelect = posibility;
						break;
					}
				}
			} else {
				return true;
			}
			
			if (posibilityToSelect.isSelected()) {
				log.info("The possibility with id " + posibilityToSelect.getId() + " has already been selectedty ");
				return false;
			} else {
				posibilityToSelect.setSelected(true);
				roulettePosilibityRepo.save(posibilityToSelect);
				return true;
			}
		} catch (ValidateServiceException | NoDataFoundException e) {
			log.info(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new GeneralServiceException(e.getMessage(), e);
		}
		
	}
}
