package com.masivian.rouletteapi.service.impl;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.masivian.rouletteapi.converters.BetConverter;
import com.masivian.rouletteapi.converters.RoulettePosibilityConverter;
import com.masivian.rouletteapi.dtos.ResultDTO;
import com.masivian.rouletteapi.entity.Bet;
import com.masivian.rouletteapi.entity.Roulette;
import com.masivian.rouletteapi.entity.RoulettePosibility;
import com.masivian.rouletteapi.entity.User;
import com.masivian.rouletteapi.exceptions.GeneralServiceException;
import com.masivian.rouletteapi.exceptions.NoDataFoundException;
import com.masivian.rouletteapi.exceptions.ValidateServiceException;
import com.masivian.rouletteapi.repository.BetRepository;
import com.masivian.rouletteapi.repository.RouletteRepository;
import com.masivian.rouletteapi.repository.UserRepository;
import com.masivian.rouletteapi.service.RouletteService;
import com.masivian.rouletteapi.util.RoulettePosibilitiesUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class RouletteServiceImpl implements RouletteService {

	@Autowired
	private RouletteRepository rouletteRepo;
	
	@Autowired
	private BetRepository betRepo;
	
	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private BetConverter betConverter;
	
	@Autowired
	private RoulettePosibilityConverter roulettePosibilityConverter;
	
	@Override
	public List<Roulette> findAll(Pageable page) {
		try {
			return rouletteRepo.findAll(page).toList();
		} catch (ValidateServiceException | NoDataFoundException e) {
			log.info(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new GeneralServiceException(e.getMessage(), e);
		}
	}

	@Override
	public Roulette findById(Long rouletteId) {
		try {
			return rouletteRepo.findById(rouletteId)
					.orElseThrow(() -> new NoDataFoundException("La Ruleta con Id: " + rouletteId + " NO existe."));
		} catch (ValidateServiceException | NoDataFoundException e) {
			log.info(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new GeneralServiceException(e.getMessage(), e);
		}
	}

	@Transactional
	@Override
	public Roulette createNewRoulette(Roulette roulette) {
		try {
			if (roulette.getId() == null) {
				// Get the roulette posibilities
				List<RoulettePosibility> posibilities = RoulettePosibilitiesUtil.getPosibilities();
				
				Roulette rouleteToSave = rouletteRepo.save(Roulette.builder()
							.regDate(LocalDateTime.now())
							.open(false)
							.roulettePosibilities(posibilities)
							.finished(false)
							.build());
				
				rouleteToSave.getRoulettePosibilities()
					.forEach(posibility -> posibility.setRoulette(rouleteToSave));
						
				log.info(rouleteToSave.toString());
				Roulette newRoulette = rouletteRepo.save(rouleteToSave);
				
				return newRoulette;
			}
			throw new  ValidateServiceException("Error creando la Ruleta");
		} catch (ValidateServiceException | NoDataFoundException e) {
			log.info(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new GeneralServiceException(e.getMessage(), e);
		}
	}

	@Transactional
	@Override
	public Roulette openRoulette(Long rouletteId) {
		try {
			Roulette savedRoulette = rouletteRepo.findById(rouletteId)
					.orElseThrow(() -> new NoDataFoundException("La Ruleta con Id: " + rouletteId + " NO existe."));
			
			if (savedRoulette.isFinished()) 
				throw new ValidateServiceException("No puede abrir una ruleta finalizada");
						
			// Change the roulette open status
			savedRoulette.setOpen(true);
			return rouletteRepo.save(savedRoulette);
		} catch (ValidateServiceException | NoDataFoundException e) {
			log.info(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new GeneralServiceException(e.getMessage(), e);
		}
	}

	
	
	@Override
	public RoulettePosibility getWinningChance(Roulette roulette) {
		Random rand = new Random();
		RoulettePosibility winningChance = roulette.getRoulettePosibilities()
					.get(rand.nextInt(roulette.getRoulettePosibilities().size()));
		
		return winningChance;
	}
	
	@Transactional
	@Override
	public ResultDTO closeRoulette(Long rouletteId) {
		try {
			Roulette savedRoulette = rouletteRepo.findById(rouletteId)
						.orElseThrow(() -> new NoDataFoundException("La Ruleta con Id: " + rouletteId + " NO existe."));
						
			// Change the roulette open status
			savedRoulette.setOpen(false);
			// Change the roulette finished status
			savedRoulette.setFinished(true);
			
			// Get the winner RoulettePosibility
			RoulettePosibility winningChance = getWinningChance(savedRoulette);
			
			log.info("Numero Ganador: " + winningChance.getNumber());
			log.info("Color Ganador: " + winningChance.getColor());
			
			// Search the winner numberBets
			List<Bet> numberBets = betRepo.findByBetTypeAndRouletteIdAndBet("NUMERO", rouletteId, String.valueOf(winningChance.getNumber()));
			// Search the winner colorBets
			List<Bet> colorBets = betRepo.findByBetTypeAndRouletteIdAndBet("COLOR", rouletteId, winningChance.getColor());
			
			// All Winners
			List<Bet> winners = Stream.concat(numberBets.stream(), colorBets.stream()).collect(Collectors.toList());
			
			// Calculate rewards
			calculateReward("NUMERO", numberBets);
			calculateReward("COLOR", colorBets);
			
			// Save States
			rouletteRepo.save(savedRoulette);
			
			return ResultDTO.builder()
					.rouletteId(rouletteId)
					.allBets(betConverter.convertEntityListToDTOList(betRepo.findByRouletteId(rouletteId)))
					.winnerPosibility(roulettePosibilityConverter.convertEntityToDTO(winningChance))
					.winnerBets(betConverter.convertEntityListToDTOList(winners))
					.build();
					
		} catch (ValidateServiceException | NoDataFoundException e) {
			log.info(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new GeneralServiceException(e.getMessage(), e);
		}
	}

	@Override
	public Roulette viewRoulettePosibilities(Long rouletteId) {
		try {
			Roulette savedRoulette = rouletteRepo.findById(rouletteId)
					.orElseThrow(() -> new NoDataFoundException("La Ruleta con Id: " + rouletteId + " NO existe."));
			
			return savedRoulette;
		} catch (ValidateServiceException | NoDataFoundException e) {
			log.info(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new GeneralServiceException(e.getMessage(), e);
		}
	}

	@Override
	public void calculateReward(String betType, List<Bet> bets) {
		try {
			double reward = 0;
			for (Bet b : bets) {
				reward = (b.getQuantity() * (betType.trim().toUpperCase().equals("NUMERO") ? 5 : 1.8));
				// Update the available money with reward
				User user = b.getUser();
				user.setMoneyAvailable(user.getMoneyAvailable() + reward);
				
				// Save
				userRepo.save(user);
			}
		} catch (ValidateServiceException | NoDataFoundException e) {
			log.info(e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new GeneralServiceException(e.getMessage(), e);
		}
	}

	

}
