package com.masivian.rouletteapi.util;

import java.util.ArrayList;
import java.util.List;

import com.masivian.rouletteapi.entity.RoulettePosibility;

public class RoulettePosibilitiesUtil {

	public static List<RoulettePosibility> getPosibilities() {
		
		List<RoulettePosibility> posibilities = new ArrayList<>();
		
		for (int x = 1; x <= 36; x++) {
			if (x % 2 == 0) {
				posibilities.add(
						RoulettePosibility.builder()
							.number(x)
							.color("ROJO")
							.selected(false)
							.build()
						);
			} else {
				posibilities.add(
						RoulettePosibility.builder()
							.number(x)
							.color("NEGRO")
							.selected(false)
							.build()
						);
			}
		}
		return posibilities;
	}
	
	
}
